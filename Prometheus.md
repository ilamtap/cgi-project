# Guidelines for Prometheus and Grafana use case

Dear Alice ,

Prometheus is One of the most popular use cases is gathering numeric metrics from a service that runs constantly. As long as the files are published on an HTTP server, you can query Prometheus by using the hostname, path, and port in order for the server to scrape the data you need. Monitoring the service metrics for an app can tell you important information about memory utilization, CPU utilization, the number of threads, and more.

The metrics are collected by vmagent and vmagent sends the metrics to vmcluster and grafana pulls them.
