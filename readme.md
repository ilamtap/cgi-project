## This is the sample app which can be deployed in our K8 cluster/minikube.

I have deployed this into my minikube and tested it.

Step 1: Please close this repo and install it via helm - Much easier approach

helm install ilam .

you can set the alias via https://ahmet.im/blog/kubectl-aliases/ if you dont have it in your lacal env.

Step 2: 

helm ls : validate the helm chart and version

step 3: 

validate your pods under the exact namespace
kgpo

step 4: 
Validate your ingress
kgsvc

step 5: access the service via test.example.com
k get ingress

update your local minikube in /etc/hosts and point to the test.example.com also add the nameserver in your minikube resolv.conf

Note: regarding the deployment stage - we need to integrate gitlab with k8, which is needed gitlab/minikube or k8 intergration.
Currently i am using minikube only for the local tests, Due to resource problem i am not running gitlab/jenkins in my minikube.
