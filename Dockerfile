FROM maven:3.8.4-jdk-11 AS build
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn -f /usr/src/app/pom.xml clean install -DskipTests

FROM openjdk:11
COPY --from=build /usr/src/app/target/*.jar /usr/app/demo-application.jar 
EXPOSE 8090
ENTRYPOINT ["java","-jar","/usr/app/demo-application.jar"]
