package com.test.example.demoapplication;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/todo")
public class TodoController {

    private final TodoRepository todoRepository;

    public TodoController(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    @GetMapping
    public Iterable<TodoEntity> todos() {
        return todoRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<TodoEntity> todo(@PathVariable int id) {
        Optional<TodoEntity> foundTodo = todoRepository.findById(id);
        if (foundTodo.isPresent()) {
            return new ResponseEntity<>(foundTodo.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping
    public void todo(@RequestBody TodoEntity todoEntity) {
        todoRepository.save(todoEntity);
    }

    @DeleteMapping("/{id}")
    public void deleteTodo(@PathVariable int id) {
        todoRepository.deleteById(id);
    }

}
